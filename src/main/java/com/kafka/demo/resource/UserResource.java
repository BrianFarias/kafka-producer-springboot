package com.kafka.demo.resource;

import com.kafka.demo.dto.UserDto;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/")
public class UserResource {

    @Autowired
    private KafkaTemplate<String, UserDto> kafkaTemplate;

    private static final String TOPIC = "Kafka_Example";

    @PostMapping("publish")
    public String post(@RequestBody UserDto user){
        kafkaTemplate.send(TOPIC, user);
        return "published Correcto";
    }


}
